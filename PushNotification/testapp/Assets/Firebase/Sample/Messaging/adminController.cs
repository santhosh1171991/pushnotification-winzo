﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Request library
using System.Net;
using System.IO;
using System;
using SimpleJSON;
using UnityEngine.UI;

namespace Firebase.Sample.Messaging
{
    public class adminController : MonoBehaviour
    {
        public InputField minScore;
        public InputField maxScore;

        public List<DataBaseTest.User> allUsers = new List<DataBaseTest.User>();

        // Use this for initialization
        void Start()
        {
            string url = @"https://hpushnotification-287db.firebaseio.com/users.json";
            //GET(url, onComplete);
            sendMessageToUser();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private string results;

        public String Results
        {
            get
            {
                return results;
            }
        }

        public WWW GET(string url, System.Action onComplete)
        {

            WWW www = new WWW(url);
            StartCoroutine(WaitForRequest(www, onComplete));
            return www;
        }

        public WWW POST(string url, Dictionary<string, string> post, System.Action onComplete)
        {
            WWWForm form = new WWWForm();

            foreach (KeyValuePair<string, string> post_arg in post)
            {
                form.AddField(post_arg.Key, post_arg.Value);
            }

            WWW www = new WWW(url, form);

            StartCoroutine(WaitForRequest(www, onComplete));
            return www;
        }

        private IEnumerator WaitForRequest(WWW www, System.Action onComplete)
        {
            yield return www;
            // check for errors
            if (www.error == null)
            {
                results = www.text;
                onComplete();
            }
            else
            {
                Debug.Log(www.error);
            }
        }

        private void onComplete()
        {
            Debug.Log("results: " + results);
            var jsonData = JSON.Parse(results);
            Debug.Log("jsonData: " + jsonData.ToString());
            foreach (JSONObject employee in jsonData)
            {
                DataBaseTest.User tempUser = new DataBaseTest.User();
                tempUser.name = employee.AsObject["name"].Value;
                tempUser.score = employee.AsObject["score"].Value;
                tempUser.Token = employee.AsObject["Token"].Value;

                allUsers.Add(tempUser);
            }
            foreach (DataBaseTest.User tempUser2 in allUsers)
            {
                //Debug.Log("name: " + tempUser2.name);
                //Debug.Log("score: " + tempUser2.score);
                //Debug.Log("Token: " + tempUser2.Token);
                if (Convert.ToInt32(tempUser2.score) >= Convert.ToInt32(minScore.text) && Convert.ToInt32(tempUser2.score) <= Convert.ToInt32(maxScore))
                {
                    sendMessageToUser(/*tempUser2.Token*/);
                }
            }
        }

        private void sendMessageToUser(/*string myToken*/)
        {
            Dictionary<string, string> postDictionary = new Dictionary<string, string>();
            postDictionary.Add("headers", "{\"content-type\": \"application/json\",\"authorization\": \"key=AAAAxzimGIs:APA91bGJKw-Pdm4mJN0j7L21h3EnWANsD-hZlp7XU30Sqe1g6GTdXj3bxL-cM_BEgzi4VnMKbdkqucLGkZJ_v2mkw72olTkb1rfzHoK3JNITGLmRApnU6m6NZAoeZnFfnUX5AgAExsSJ\"}");
            postDictionary.Add("payload", "{\"notification\":{\"title\":\"Test\",\"body\":\"TestMessage\",\"mutable_content\":true},\"to\":\"fjIe7HhD9tw:APA91bHNtUA2HUXwaazNVM7P0z5VHuFJINIHqJYjNnFyr4jnLl0_tmPYY6GfNx_gbJVjNmcCc4hENdgvFKF_CcIF-AQWF1HWM8LBhXEn8LtGEj9p60apumMUgtjyTvSujUhPc6TEdUdw\"}");
            string url = @"https://fcm.googleapis.com/fcm/send";
            POST(url, postDictionary, onComplete2);
        }

        private void onComplete2()
        {
        }
    }
}
