﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;


namespace Firebase.Sample.Messaging
{
    public class DataBaseTest : MonoBehaviour
    {


        public InputField Username;
        public InputField UserScore;
        public DatabaseReference reference;
        public UIHandler myUIHandler;

        public class User
        {
            public string score;
            public string name;
            public string Token;

            public User()
            {
            }

            public User(string score, string name, string Token)
            {
                this.score = score;
                this.name = name;
                this.Token = Token;
            }
        }

        // Use this for initialization
        void Start()
        {

            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://hpushnotification-287db.firebaseio.com/");

            // Get the root reference location of the database.
            reference = FirebaseDatabase.DefaultInstance.RootReference;
            Debug.Log("Reference: " + reference);

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void writeNewUser()
        {
            User user = new User(UserScore.text, Username.text, myUIHandler.myToken);
            string json = JsonUtility.ToJson(user);

            reference.Child("users").Child(Username.text).SetRawJsonValueAsync(json);
            Debug.Log("writeNewUser info:" + json);
        }
    }
}